// Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef _BLE_GAMEPAD_H_
#define _BLE_GAMEPAD_H_

#include "Arduino.h"

#include "BLECharacteristic.h"
#include "BLEHIDReportReferenceDescriptor.h"
#include "BLEHID.h"

// From: https://github.com/adafruit/Adafruit-Trinket-USB/blob/master/TrinketHidCombo/TrinketHidCombo.h
//       permission to use under MIT license by @ladyada (https://github.com/adafruit/Adafruit-Trinket-USB/issues/10)

// DPAD Bitmasks
#define DPAD_RIGHT  0x01
#define DPAD_LEFT   0x03
#define DPAD_UP     0x0C
#define DPAD_DOWN   0x04

#define BUTTON_A 0x01
#define BUTTON_B 0x02
#define BUTTON_X 0x04
#define BUTTON_Y 0x08

#define BUTTON_L 0x10
#define BUTTON_R 0x20

#define BUTTON_SELECT 0x40
#define BUTTON_START 0x80

struct gamepad_report_t
{
    uint8_t dpad_up;
    uint8_t dpad_down;
    uint8_t dpad_left;
    uint8_t dpad_right;

    uint8_t x;
    uint8_t y;
    uint8_t a;
    uint8_t b;
    
    uint8_t l;
    uint8_t r;

    uint8_t select;
    uint8_t start;

    bool operator==(gamepad_report_t& other);
    bool operator!=(gamepad_report_t& other);

};

class BLEGamepad : public BLEHID
{
  public:
    BLEGamepad();
    virtual void press(gamepad_report_t& report);

  protected:
    virtual void setReportId(unsigned char reportId);
    virtual unsigned char numAttributes();
    virtual BLELocalAttribute** attributes();

  private:
    void sendValue();

  private:
    BLECharacteristic                 _reportCharacteristic;
    BLEHIDReportReferenceDescriptor   _reportReferenceDescriptor;

    unsigned char                     _value[8];
};

#endif
