// Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "BLEGamepad.h"

/*static const PROGMEM unsigned char descriptorValue[] = {
 0x05, 0x01,         // USAGE_PAGE (Generic Desktop)
   0x09, 0x05,         // USAGE (Game Pad)
   0xa1, 0x01,         // COLLECTION (Application)
   0xa1, 0x00,         //   COLLECTION (Physical)
   0x05, 0x09,         //      USAGE_PAGE (Button)
   0x19, 0x01,         //      USAGE_MINIMUM (Button 1)
   0x29, 0x02,         //      USAGE_MAXIMUM (Button 2)
   0x15, 0x00,         //      LOGICAL_MINIMUM (0)
   0x25, 0x01,         //      LOGICAL_MAXIMUM (1) 
   0x95, 0x02,         //      REPORT_COUNT (2)
   0x75, 0x01,         //      REPORT_SIZE (1)
   0x81, 0x02,         //      INPUT (Data,Var,Abs)
   0xc0,            //   END_COLLECTION
   0xc0            // END_COLLECTION
};*/

/* The Pad Report Descriptor */
static const PROGMEM unsigned char descriptorValue[] = {
   0x05, 0x01, // Usage Page (Generic Desktop),
   0x09, 0x05, // 	Usage (Game Pad),
   0xa1, 0x01, // 		Collection (Application),

   // Pointer 	
   0x09, 0x01, //   Usage (Pointer),
   0xa1, 0x00, //   Collection (Physical),
   0x09, 0x30, //     Usage (X),
   0x09, 0x31, //     Usage (Y),
   0x15, 0xff, //     Logical Minimum (-1),
   0x25, 0x01, //     Logical Maximum (+1),
   0x95, 0x02, //     Report Count (2),
   0x75, 0x02, //     Report Size (2),
   0x81, 0x02, //     Input (Data, Variable, Absolute, No Null),
   0xc0,       //   End Collection,
   
   // Padding
   0x95, 0x04, //   Report Count (4),
   0x75, 0x01, //   Report Size (1),
   0x81, 0x03, //   Input (Constant, Variable, Absolute),

   // 8 Buttons
   0x05, 0x09, //   Usage Page (Buttons),
   0x19, 0x01, //   Usage Minimum (Button 1),
   0x29, 0x08, //   Usage Maximum (Button 8),
   0x15, 0x00, //   Logical Minimum (0),
   0x25, 0x01, //   Logical Maximum (1),
   0x95, 0x08, //   Report Count (8),
   0x75, 0x01, //   Report Size (1),
   0x81, 0x02, //   Input (Data, Variable, Absolute, No Null),

   0xc0        // End Collection
};

BLEGamepad::BLEGamepad() :
  BLEHID(descriptorValue, sizeof(descriptorValue), 0),
  _reportCharacteristic("2a4d", BLERead | BLENotify, 8), // 2A4D = Report Characteristics
  _reportReferenceDescriptor(BLEHIDDescriptorTypeInput)
{
  memset(this->_value, 0, sizeof(this->_value));
}

void BLEGamepad::press(gamepad_report_t& report) {
  for (int i = 0; i < sizeof(this->_value); i++) {
    this->_value[i] = 0;
  }

  this->_value[0] |= report.dpad_up;
  this->_value[0] |= report.dpad_down;
  this->_value[0] |= report.dpad_left;
  this->_value[0] |= report.dpad_right;

  this->_value[1] |= report.b;
  this->_value[1] |= report.y;
  this->_value[1] |= report.select;
  this->_value[1] |= report.start;
  this->_value[1] |= report.a;
  this->_value[1] |= report.x;
  this->_value[1] |= report.l;
  this->_value[1] |= report.r;  
  
  this->sendValue();
}

void BLEGamepad::setReportId(unsigned char reportId) {
  BLEHID::setReportId(reportId);

  this->_reportReferenceDescriptor.setReportId(reportId);
}

unsigned char BLEGamepad::numAttributes() {
  return 2;
}

BLELocalAttribute** BLEGamepad::attributes() {
  static BLELocalAttribute* attributes[2];

  attributes[0] = &this->_reportCharacteristic;
  attributes[1] = &this->_reportReferenceDescriptor;

  return attributes;
}

void BLEGamepad::sendValue() {
  BLEHID::sendData(this->_reportCharacteristic, this->_value, sizeof(this->_value));
}

bool gamepad_report_t::operator==(gamepad_report_t& other) {
    bool same = true;
    same &= dpad_up == other.dpad_up;
    same &= dpad_down == other.dpad_down;
    same &= dpad_left == other.dpad_left;
    same &= dpad_right == other.dpad_right;

    same &= x == other.x;
    same &= y == other.y;
    same &= a == other.a;
    same &= b == other.b;
    same &= l == other.l;
    same &= r == other.r;
    same &= select == other.select;
    same &= start == other.start;

    return same;
}

bool gamepad_report_t::operator!=(gamepad_report_t& other) {
    return !operator==(other);
} 