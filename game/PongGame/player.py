from kivy.properties import NumericProperty
from kivy.uix.widget import Widget
from kivy.vector import Vector


class PongPaddle(Widget):
    score = NumericProperty(0)
    is_horizontal = False
    paddle_velocity = (0, 0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def bounce_ball(self, ball):
        offset_y = (ball.center_y - self.center_y) / (self.height / 2)
        offset_x = (ball.center_x - self.center_x) / (self.width / 2)
        vx, vy = ball.velocity
        vx, vy = self.increase_ball_speed(vx, vy)

        if self.is_horizontal:
            vy *= -1
            ball.velocity = vx + offset_x, vy
        else:
            vx *= -1
            ball.velocity = vx, vy + offset_y

    @staticmethod
    def increase_ball_speed(vx, vy, speed_increase=1.06, max_speed=10):
        if -max_speed < vy < max_speed and -max_speed < vx < max_speed:
            vy *= speed_increase
            vx *= speed_increase
        return vx, vy

    def is_colliding(self, wid):
        if self.right < wid.x and (self.right - (self.width / 2)):
            return False
        if self.x > wid.right:
            return False
        if self.top < wid.y:
            return False
        if self.y > wid.top:
            return False
        return True

    def move_paddle(self, x, y, width, height):
        if self.is_horizontal:
            if self.paddle_velocity[0] > 0 and (self.x + self.width) >= width:
                self.paddle_velocity = (0, 0)
            elif self.paddle_velocity[0] < 0 and self.x <= x:
                self.paddle_velocity = (0, 0)
        else:
            if self.paddle_velocity[1] > 0 and (self.y + self.height) >= height:
                self.paddle_velocity = (0, 0)
            elif self.paddle_velocity[1] < 0 and self.y <= y:
                self.paddle_velocity = (0, 0)

        self.pos = Vector(*self.paddle_velocity) + self.pos

    def disable(self):
        self.disabled = True
