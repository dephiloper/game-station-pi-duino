#!/usr/bin/env python
# import kivy
# kivy.require(1.9)
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, \
    ObjectProperty
from kivy.vector import Vector
from kivy.clock import Clock
from player import PongPaddle
from game_input import InputManager, Direction
import random


class PongBall(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x, velocity_y)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos


class PongGame(Widget):
    # Objects
    My_Clock = Clock
    ball = ObjectProperty(PongBall)
    player1 = ObjectProperty(PongPaddle)
    player2 = ObjectProperty(PongPaddle)
    player3 = ObjectProperty(PongPaddle)
    player4 = ObjectProperty(PongPaddle)

    players = ReferenceListProperty(player1, player2, player3, player4)

    winner_label = None
    last_collided_id = 1
    default_speed = 4
    max_score = 9

    def start_game(self):
        print("initialized")
        for player in self.players:
            player.score = 0
        self.serve_ball()
        self.player3.is_horizontal = True
        self.player4.is_horizontal = True

    def end_game(self, won_player_id):
        self.My_Clock.unschedule(self.update)
        self.winner_label = Label(text="Player " + str(won_player_id + 1) + " gewinnt.", font_size='40sp',
                                  pos=(self.center_x - 50, self.center_y + 50))
        self.add_widget(self.winner_label)

        self.My_Clock.schedule_once(self.pause_game, 3)
        self.start_game()

    def pause_game(self, dt):
        self.My_Clock.schedule_interval(self.update, 1/60)
        self.remove_widget(self.winner_label)

    # ball reset
    def serve_ball(self):
        self.ball.center = self.center
        self.ball.velocity = PongGame.direction_to_velocity(random.choice([Direction.FORWARD, Direction.BACKWARD]),
                                                            random.choice([True, False]))

        # self.ball.velocity = (-10, 0)

    def update(self, dt):
        self.ball.move()

        # Bewegung der paddles
        for player in self.players:
            player.move_paddle(self.x, self.y, self.width, self.height)

        colliding_player_id = self.check_collision()

        current_player = self.players[colliding_player_id];

        if not colliding_player_id == -1:
            current_player.bounce_ball(self.ball)
            self.last_collided_id = colliding_player_id

        self.ball_out_of_court(self.players[self.last_collided_id])

    def check_collision(self):
        for i in range(0, len(self.players)):
            p = self.players[i]
            if p.is_colliding(self.ball):
                return i

        return -1

    def ball_out_of_court(self, last_collided_player):
        if self.ball.x < self.x \
                or self.ball.x + self.ball.width > self.width \
                or self.ball.y < self.y \
                or self.ball.y + self.ball.width > self.height:
            if last_collided_player.score < self.max_score:
                last_collided_player.score += 1
                self.serve_ball()
            else:
                self.end_game(self.last_collided_id)

    # debug
    def on_touch_move(self, touch):
        if touch.x < self.width / 3:
            self.player1.center_y = touch.y
        if touch.x > self.width - self.width / 3:
            self.player2.center_y = touch.y

    # richtung der paddles ueber controllerinput
    def on_joy_axis(self, win, stickid, axisid, value):
        current_player = self.players[stickid]
        direction_value = InputManager.axis_to_direction(value, current_player.is_horizontal, axisid)
        if direction_value is not None:
            current_player.paddle_velocity = self.direction_to_velocity(direction_value, current_player.is_horizontal)

    def on_joy_button_down(self, win, stickid, buttonid):
        print("button down")
        current_player = self.players[stickid]
        direction_value = InputManager.button_to_direction(buttonid)
        current_player.paddle_velocity = self.direction_to_velocity(direction_value, current_player.is_horizontal)

    def on_joy_button_up(self, win, stickid, buttonid):
        print("button up")
        current_player = self.players[stickid]
        current_player.paddle_velocity = self.direction_to_velocity(Direction.NONE, current_player.is_horizontal)

    def on_joy_hat(self, win, stickid, hatid, value):
        current_player = self.players[stickid]
        direction_value = InputManager.hat_to_direction(value, current_player.is_horizontal)
        current_player.paddle_velocity = self.direction_to_velocity(direction_value, current_player.is_horizontal)

    # keyboard down event
    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        print("keyboard down triggered")
        current_player = self.players[0]
        direction_value = InputManager.key_to_direction(keycode[0], current_player.is_horizontal)
        current_player.paddle_velocity = self.direction_to_velocity(direction_value, current_player.is_horizontal)

    # keyboard up event
    def _on_keyboard_up(self, keyboard, keycode):
        print("keyboard up triggered")
        current_player = self.players[0]
        current_player.paddle_velocity = self.direction_to_velocity(Direction.NONE, current_player.is_horizontal)

    # keyboard unbind
    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    # Binding fuer das offene Fenster
    def bind_window(self):
        Window.bind(on_joy_axis=self.on_joy_axis)
        Window.bind(on_joy_button_down=self.on_joy_button_down)
        Window.bind(on_joy_button_up=self.on_joy_button_up)
        Window.bind(on_joy_hat=self.on_joy_hat)

        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_keyboard_up)

    @staticmethod
    def direction_to_velocity(direction, is_horizontal=False):
        if direction == Direction.NONE:
            return 0, 0
        if direction == Direction.FORWARD:
            if is_horizontal:
                return PongGame.default_speed, 0
            else:
                return 0, PongGame.default_speed
        if direction == Direction.BACKWARD:
            if is_horizontal:
                return -PongGame.default_speed, 0
            else:
                return 0, -PongGame.default_speed


class PongApp(App):
    def build(self):
        game = PongGame()
        game.start_game()
        game.bind_window()
        game.My_Clock = Clock
        game.My_Clock.schedule_interval(game.update, 1.0 / 60.0)
        return game


if __name__ == '__main__':
    PongApp().run()
