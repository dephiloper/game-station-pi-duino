from enum import Enum


class InputManager:

    @staticmethod
    def hat_to_direction(hat_value, is_horizontal):
        if is_horizontal:
            if hat_value == DPad.LEFT.value:
                return Direction.BACKWARD
            elif hat_value == DPad.RIGHT.value:
                return Direction.FORWARD
        else:
            if hat_value == DPad.UP.value:
                return Direction.FORWARD
            elif hat_value == DPad.DOWN.value:
                return Direction.BACKWARD
        return Direction.NONE

    @staticmethod
    def button_to_direction(button_id):
        if button_id == Button.A.value:
            return Direction.FORWARD
        elif button_id == Button.B.value:
            return Direction.BACKWARD

        return Direction.NONE

    @staticmethod
    def axis_to_direction(joy_value, is_horizontal, axis_id):
        deviation_threshold = 1000

        if not axis_id == InputManager.align_to_axis(is_horizontal):
            print("axis rejected")
            return None

        # if not -deviation_threshold < joy_value < deviation_threshold:
        print("valid")
        if joy_value < -6000:
            return Direction.FORWARD
        elif joy_value > 6000:
            return Direction.BACKWARD
        else:
            return Direction.NONE
        #else:
        #    print("threshold rejected")
        #    return None

    @staticmethod
    def key_to_direction(key_code, is_horizontal):
        if is_horizontal:
            if key_code == KeyCode.LEFT.value:
                return Direction.BACKWARD
            elif key_code == KeyCode.RIGHT.value:
                return Direction.FORWARD
        else:
            if key_code == KeyCode.UP.value:
                return Direction.FORWARD
            elif key_code == KeyCode.DOWN.value:
                return Direction.BACKWARD
        return Direction.NONE

    @staticmethod
    def align_to_axis(is_horizontal):
        if is_horizontal:
            return 0
        else:
            return 1


class DPad(Enum):
    NONE = (0, 0)
    UP = (0, 1)
    DOWN = (0, -1)
    LEFT = (-1, 0)
    RIGHT = (1, 0)


class KeyCode(Enum):
    UP = 119
    DOWN = 115
    LEFT = 97
    RIGHT = 100


class Button(Enum):
    A = 0
    B = 1


class Direction(Enum):
    BACKWARD = -1
    NONE = 0
    FORWARD = 1
