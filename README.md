# Gamestation

[TOC]

##Introduction
Dieses Projekt wurde im Rahmen des Wahlpflichtmoduls "Mobile Anwendungen - Drahtlose Netzwerke" der HTW-Berlin erstellt. Es handelt sich hierbei um die Abstraktion einer Spielkonsole, auf der ein Pong-Spiel ausgeführt wird. Mithilfe von maximal vier Game Controllern ist es möglich, entsprechend viele Spieler im benannten Spiel interagieren zu lassen. 

##Components
Das Projekt setzt sich aus folgenden technischen Geräten zusammen:

 1. Raspberry Pi 3
 2. RedBearLab BLE Nano Kit v2
 3. 2x RedBearLab BLE Nano v2
 4. 3x Super Nintendo Entertainment System (SNES) Controller

##Description
Auf dem oben genannten Raspberry Pi 3 fungiert Arch Linux ARM (32-bit) als Betriebssystem. Das in dem Projekt inkludierte Spiel ist auf dem Einplatinencomputer installiert und kann nach dem Hochfahren der "Gamestation" gestartet werden. Die für das Spiel benötigten Controller können nun manuell über Bluetooth oder Kabel mit dem Raspberry Pi verbunden werden, insofern dies nicht simultan mit dem Start des Spiels geschehen sein sollte. Es können parallel bis zu vier Inputs sinnvoll im Spiel abgebildet und ausgewertet werden. Jenes erfüllt die generellen Anforderungen eines 2D-Pong-Spiels, erweitert das originale Konzept jedoch um zwei weitere Spieler. Das Basiskonzept bleibt erhalten - ein Ball wird zwischen den Spielern immer schneller hin und her gespielt. Dabei muss jeder Teilnehmer darauf achten, dass der Ball nicht den eigenen, lokalen Bildschirmrand erreicht. Sollte dies dennoch geschehen, bekommt der gegnerische Spieler, der die letzte Berührung durchgeführt hat, einen Punkt.
Des Weiteren sieht das Projekt drei selbst gebaute Controller vor. Diese sollen eine Erweiterung der klassischen SNES Controller darstellen, welche allerdings BLE zur Datenübertragung nutzt. Zur Realisierung der BLE Verbindung werden die anfangs erwähnten "Red Bear Boards" mit der Original Platine verlötet. Dies wurde für einen Controller realisiert und für diesen Prototypen der volle Funktionsumfang implementiert.
Weiterführend ist geplant, die Boards in das Gehäuse einzubauen und mit hilfe eines kleinen Akkus, die Stromversorgung bereitzustellen. Der Bau der zwei weiteren Controller wird außerhalb der Projektzeit realisiert.

###Controls
Die Steuerung des Pong-Spiels erfolgt entsprechend der verbundenen Input Geräte. Hierbei werden folgende von der "Gamestation" unterstützt:

 - Tastatur (eingeschränkt nutzbar)
 - Touchpad
 - Xbox One Controller (BLE Modus)
 - **Kabelgebundener** Xbox 360 Controller
 - Playstation 4 Controller
 - jegliche Art von **kabelgebunden** Controllern, die korrekt konfiguriert wurden

Grundlegend wurde definiert, dass jeder Spieler einen Steuerknüppel erhält, den er kontrollieren kann. Die eigentliche Steuerung erfolgt auf der Tastatur mittels "w" und "s" bzw. "a" und "d", entsprechend der horizontalen bzw. vertikalen Ausrichtung des zugeordneten grafischen Elements.
Dieses Vorgehen wird auf die Steuerung mit einem Controller übertragen, wodurch dem Spieler ermöglicht wird, die beschriebene Funktion mit dem DPad oder Joystick auszuführen.

###Implementation
####Pong-Game
Das Pong Spiel wurde unter Nutzung von Kivy 1.9 in Python 3.6.1 entwickelt. Kivy ist eine Open Source Cross-platform Game-Engine bzw. ein UI Framework.  

#####Files
 - pong_app.py
	 - "PongGame" - Klasse mit der Spiellogik und Gameloop
	 - "PongBall" - zusätzliche Klasse für den Ball
	 - "PongApp" - Build - Klasse
 - player.py
	 - "PongPaddle" - Klasse für vier Spieler, die über jene instanziiert werden
 - game_input.py
	 - "InputManager" - statische Klasse, welche die Eingabe jedes einzelnen Gerätes verarbeitet
	 - Enums:
		 - "DPad"
		 - "KeyCode"
		 - "Button"
		 - "Direction"
 - pong.kv
	 - Kivy-Datei für das Definieren von Attributen für die grafisch zu visualisierenden Elemente
	 - nur mit der geforderten oder aktuelleren Kivy-Version nutzbar, andernfalls ist das Programm generell nicht ausführbar


##### Functionality
Das Spiel arbeitet auf der Basis des Kivy - Frameworks. Einleitend sei gesagt, dass sich Kivy Widgets, die grafischen Elemente, die im .kv-File definiert werden, nicht wie herkömmliche Objekte behandelt werden können. Widgets, die jeweils als Objekt dargestellt und angesprochen werden können sollen, werden als sogenannte "ObjectProperty" (from kivy.properties) instanziert.

```
ball = ObjectProperty(PongBall)
player1 = ObjectProperty(PongPaddle)
    ...
players = ReferenceListProperty(player1, player2, player3, player4)
```
Die "ReferenceListProperty" ist hierbei eine Art Liste für die Kivy-Objekte, kann aber auch als Tupel genutzt werden.
Die Build - Klasse des pong_app.py Moduls führt folgende Funktionen des "PongGame(s)" aus:

```
game = PongGame()
game.start_game()
```
Initialisierung und Start des Spiels (dient unter anderem auch als Neustart des Spiels).

```
game.bind_window()
```
Definiert die Eingabegeräte (Controller, Tastatur, Touchpad, etc.), welche für das Spiel erlaubt sind. Die Eingaben werden durch das SDL2 Framework aufgenommen und anhand der vorher definierten Eingabegeräte gefiltert.
```
game.My_Clock = Clock
game.My_Clock.schedule_interval(game.update, 1.0 / 60.0)
```
Kivy besitzt eine interne Klasse, die als eine Art Uhr fungieren kann. Die hier gezeigte Anwendung der sogenannten "Clock" setzt den Aufruf Zyklus der Game Loop - Funktion (game.update) auf 60 mal pro Sekunde (Aufruf der Methode einmal alle 16,67 Millisekunden).

Über die "update" - Methode wird jedes Mal, wenn diese durchlaufen wird, von dem Ball und den Spielern die Geschwindigkeit (velocity) abgefragt und diese in die entsprechende Richtung bewegt. Zusätzlich wird fortlaufend beim Spieler abgefragt, ob der Ball auf ihn trifft (check_collision() & is_colliding()). Sollte dies der Fall sein, so wird die ID des Spielers durch die "players" - Liste zurückgegeben und folgende Methode bei diesem Spieler aufgerufen:

```
def bounce_ball(self, ball):
    offset_y = (ball.center_y - self.center_y) / (self.height / 2)
    offset_x = (ball.center_x - self.center_x) / (self.width / 2)
    ...
```
Hierbei wird die Relation des Zentrums des Balls zu dem des "PongPaddles"  berechnet      

```
	...
    if self.is_horizontal:
        vy *= -1
        ball.velocity = vx + offset_x, vy
    else:
        vx *= -1
        ball.velocity = vx, vy + offset_y
```
und anschließend zu der neu angepassten Geschwindigkeit hinzuaddiert, um den Ball entsprechend realistisch von dem Paddle abprallen zu lassen.

Der "InputManager" ist, wie bereits erwähnt, eine Klasse, die ausschließlich statische Methoden besitzt. Ein Hauptproblem, das bei den vordefinierten "Input - Funktionen" von Kivy auftritt, besteht darin, dass diese Parameter übergeben bekommen, die alle vollkommen unabhängig voneinander und damit nicht leicht vergleichbar sind. Daraus folgend muss beim "InputManager" für jeden Input, der an das Fenster gebunden wurde, ebenfalls eine Methode implementiert werden. 

```
@staticmethod
def hat_to_direction(hat_value, is_horizontal):
```
Hierbei handelt es sich um das Beispiel von dem DPad, dessen Methode über die Funktion "on_joy_hat()" der "PongGame" - Klasse aufgerufen wird. "hat_to_direction()" bekommt den Wert der gedrückten DPad - Richtung und einen Boolean von dem entsprechenden Spieler mit.

```
if is_horizontal:
    if hat_value == DPad.LEFT.value:
        return Direction.BACKWARD
    elif hat_value == DPad.RIGHT.value:
        return Direction.FORWARD
    ...    
```
	            
Wenn der Spieler horizontal ist, wird weiterführend geprüft, ob das DPad in die linke oder rechte Richtung gedrückt wurde. Bei dem "hat_value" handelt es sich um einen Tupel, der mit dem Enum - Wert abgeglichen wird, um die korrekte Richtung zurückgeben zu können. Anzufügen ist, dass es nur die Richtungen vorwärts, rückwärts und none gibt.

####BLE-Controller
Zu Beginn des Projektes wurde als Entwicklungsplattform auf die bereits von der HTW vorhandenen RFduino Boards gesetzt. Bei diesen zeigte sich allerdings schnell, dass sie den Ansprüchen des Projektes bezüglich der Programmierbarkeit nicht gerecht wurden. Mit deren Standardbibliothek ist es z.B. nicht möglich [GATT](https://learn.adafruit.com/introduction-to-bluetooth-low-energy/gatt) Characteristics und GATT Services zu setzen.

Zur Lösung dieses Problems diente die [BLE Peripheral Library](https://github.com/sandeepmistry/arduino-BLEPeripheral). Diese Bibliothek ist eine Nachentwicklung der Bibliothek des Chip Herstellers (Nordic Semiconductor) und erlaubt die Nutzung der Hardware in ihrem ganzen Umfang.

Sie ermöglichte nun die Entwicklung der BLE Komponente unseres Projektes. Hierfür wurde sie um die Implementierung des HID Gamepads erweitert (BLEGamepad.h, BLEGamepad.cpp). Diese integriert sich nahtlos in die Struktur der Bibliothek und bildet eine Abstraktion der eigentlichen Nutzung der Hardware. Innerhalb der BLEGamepad.cpp Datei werden alle benötigten GATT Characteristics, Profile und Services aktiviert. Darin enthalten ist der [HID Descriptor](http://www.usb.org/developers/hidpage/), welcher zur Deutung der Signale benötigt wird.

Aufgrund der Baugröße und der Leistung der RFduinos wurden diese später durch BLE Nano v2 Boards von [Red Bear Lab](http://redbearlab.com/) ersetzt. Letztere setzen auf den Nachfolger des bei den RFduino eingesetzten Prozessors und haben einen deutlich kleineren Formfaktor sowie eine offene API.

#####Details
Der HID Descriptor ist für unseren [SNES-Controller](https://web.archive.org/web/20151213084751/http://www.gifford.co.uk/~coredump/gpad.htm) wie folgt aufgebaut:
```
/* The Pad Report Descriptor */
static const PROGMEM unsigned char descriptorValue[] = {
   0x05, 0x01, // Usage Page (Generic Desktop),
   0x09, 0x05, // Usage (Game Pad),
   0xa1, 0x01, // Collection (Application),

   // Pointer 	
   0x09, 0x01, //   Usage (Pointer),
   0xa1, 0x00, //   Collection (Physical),
   0x09, 0x30, //     Usage (X),
   0x09, 0x31, //     Usage (Y),
   0x15, 0xff, //     Logical Minimum (-1),
   0x25, 0x01, //     Logical Maximum (+1),
   0x95, 0x02, //     Report Count (2),
   0x75, 0x02, //     Report Size (2),
   0x81, 0x02, //     Input (Data, Variable, Absolute, No Null),
   0xc0,       //   End Collection,
   
   // Padding
   0x95, 0x04, //   Report Count (4),
   0x75, 0x01, //   Report Size (1),
   0x81, 0x03, //   Input (Constant, Variable, Absolute),

   // 8 Buttons
   0x05, 0x09, //   Usage Page (Buttons),
   0x19, 0x01, //   Usage Minimum (Button 1),
   0x29, 0x08, //   Usage Maximum (Button 8),
   0x15, 0x00, //   Logical Minimum (0),
   0x25, 0x01, //   Logical Maximum (1),
   0x95, 0x08, //   Report Count (8),
   0x75, 0x01, //   Report Size (1),
   0x81, 0x02, //   Input (Data, Variable, Absolute, No Null),

   0xc0        // End Collection
};
```

Die folgenden GATT Services sowie GATT Characteristics müssen für die Verwendung eines BLE Controllers als HID Gerät aktiviert bzw. gefüllt werden. 

#####GATT Services

- Für alle BLE Peripherals benötigt:
    - Generic Access (0x1800): Allgemeine Informationen über das Gerät
    - Generic Attribute (0x1801): Der Basis Service für GATT (alle anderen benötigten diesen)
- Für alle BLE HID Peripherals benötigt:
    - Human Interface Device(0x1812): Eigenschaften welche für die Kommunikation als HID-Gerät benötigt werden

#####GATT Characteristics

- Für alle BLE Peripherals benötigt:
    - Device Name (0x2A00): Der Name, den der Controller bei der Ankündigung verwendet 
    - Appearance (0x2A01): Spezifikation, um welche Art von Gerät es sich handelt (Entscheidet über die Icon Auswahl)
    - Service Changed (0x2A05): Gibt an, ob sich der zugrunde liegende Service geändert hat
    - Peripheral Privacy Flag (0x2A02): Aktiviert oder deaktiviert das Privacy Feature von BLE (wenn aktiviert muss auch Reconnection Address gefüllt werden)
    - Reconnection Address (0x2A03): Die Adresse, unter welcher das Gerät erreicht werden kann nach einem Disconnect (wird nur von dem Privacy Feature von BLE benutzt)
    - Peripheral Preferred Connection Parameters (0x2A04)
- Für alle BLE HID Peripherals benötigt:
    - Protocol Mode (0x2A4E): Setzt die verwendete Variante des HID Protokolls
    - Report Map (0x2A4B): Zusätzliche Informationen zu dem HID Gerät
    - Boot Keyboard Input Report (0x2A22): Gibt an, ob das HID Gerät zur Bootzeit zur Verfügung steht, wenn es eine Tastatur ist
    - Boot Keyboard Output Report (0x2A32): Gibt an, ob das HID Gerät zur Bootzeit zur Verfügung steht, wenn es eine Ausgabe hat
    - Boot Mouse Input Report (0x2A33): Gibt an, ob das HID Gerät zur Bootzeit zur Verfügung steht, wenn es eine Maus ist
    - HID Information (0x2A4A): Zusätzliche Informationen zu dem HID Gerät (USB HID Protokoll Version, Länderkürzel und Verbindungsmöglichkeiten)
    - HID Control Point (0x2A4C): Gibt an, ob das HID Gerät sich gerade im Ruhemodus befindet 
- Für unser Gamepad benötigt: 
    - Report (0x2A4D): Der HID Report, welcher zur Deutung der Daten verwendet wird

Die Anbindung an den SNES-Controller wurde innerhalb des Arduino Projektes umgesetzt. Hierzu musste das SNES-Controller-Protokoll implementiert werden.  Hierzu wurde die folgende readGamepadReport() Methode implementiert.

```
gamepad_report_t readGamepadReport() {
  gamepad_report_t report = {};
  int arr[16];
  
  digitalWrite(DATA_LATCH, HIGH);
  delayMicroseconds(12);
  digitalWrite(DATA_LATCH, LOW);

  // 1. Cycle
  arr[0] = digitalRead(SERIAL_DATA);
  
  for (int i = 1; i < 16; i++) {
      digitalWrite(DATA_CLOCK, HIGH);
      delayMicroseconds(3);
      arr[i] = digitalRead(SERIAL_DATA);
      delayMicroseconds(3);
      digitalWrite(DATA_CLOCK, LOW);
      delayMicroseconds(6);    
  }
}
```

Der genaue Aufbau dieses Protokolls ist auf [Super Nintendo Entertainment System: pinouts & protocol](https://www.gamefaqs.com/snes/916396-super-nintendo/faqs/5395?print=1) einzusehen.

#####Dev Board
![Image of the development board](https://bytebucket.org/dephiloper/game-station-pi-duino/raw/9e85d3cdfc363ffd008454e124028646e5d2de90/images/test_build.jpg)

#####Wiring Diagram
![Image of the wiring diagram](https://bytebucket.org/dephiloper/game-station-pi-duino/raw/08149829f33e9a9f599f61a566e22cabaed44b03/images/wires_controller.png)


##References
####[Red Bear Lab](http://redbearlab.com/)
####[Raspberry Pi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/)
####[Kivy](https://kivy.org/#home)
####[BLE Peripheral Library](https://github.com/sandeepmistry/arduino-BLEPeripheral)
####[Super Nintendo Entertainment System: pinouts & protocol](https://www.gamefaqs.com/snes/916396-super-nintendo/faqs/5395?print=1)
#####[Image of SNES Controller](http://orig07.deviantart.net/96a2/f/2008/068/b/d/super_nintendo_controller_by_oloff3.png)

##Contributors
Die Ersteller dieses Projektes sind R. Schlett, P. Bönsch und R. Wegner-Repke.

##License
Bei diesem Projekt wurden folgende Programmiersprachen genutzt:

 1. Für die Controller - C++ (2011)
 2. Für das Pong-Spiel - Python 3.6.1; Kivy 1.9
